from __future__ import print_function
# ------------------------------------------------------------------------------------------------
# Copyright (c) 2016 Microsoft Corporation
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
# associated documentation files (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge, publish, distribute,
# sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all copies or
# substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT
# NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
# DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# ------------------------------------------------------------------------------------------------

# Tutorial sample #2: Run simple mission using raw XML

from builtins import range
from mcpi.minecraft import Minecraft
import MalmoPython
import os
import sys
import time
import random
import numpy as np
import json



items = [
    "rabbit",
    "fish",
    "cooked_beef",
    "cooked_porkchop",
    "chicken",
    "mutton",
    "egg",
    "carrot",
    "potato",
    "apple",
    "melon",
    "pumpkin_pie",
    "cookie",
    "cake",
    "sugar"
]

MOVEMENTS = {
        "UP": "move 1",
        "RIGHT": "strafe -1",
        "LEFT": "strafe 1",
        "DOWN": "move -1"
    }

if sys.version_info[0] == 2:
    sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)  # flush print output immediately
else:
    import functools
    print = functools.partial(print, flush=True)


# Var for create_items
nbr_items = 10
min_x = -10
max_x = -min_x
min_z = -10
max_z = -min_z


def create_items():
    list_items_created = ""
    for i in range(nbr_items):
        x = random.randint(min_x, max_x)
        z = random.randint(min_z, max_z)
        list_items_created += '<DrawItem x="{x}" y="250" z="{z}" type="{items_generated}" />\n'.format(
            x=x, z=z, items_generated=random.choice(items)
        )
    print(list_items_created)
    return list_items_created


# Q learning variable
# states_n = max_x * max_x
states_n = 50 * 50
actions_n = len(MOVEMENTS)
Q = np.zeros([states_n, actions_n])
lr = .85
y = .99
num_episodes = 1000
wrong_action_p = 0.1
cumul_reward_list = []
actions_list = []
states_list = []

# More interesting generator string: "3;7,44*49,73,35:1,159:4,95:13,35:13,159:11,95:10,159:14,159:6,35:6,95:6;12;"

missionXML='''<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<Mission xmlns="http://ProjectMalmo.microsoft.com" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <About>
      <Summary>Healthy diet. Eating right and wrong objects</Summary>
    </About>
    <ServerSection>
      <ServerInitialConditions>
            <Time>
                <StartTime>6000</StartTime>
                <AllowPassageOfTime>false</AllowPassageOfTime>
            </Time>
            <Weather>clear</Weather>
            <AllowSpawning>false</AllowSpawning>
      </ServerInitialConditions>
      <ServerHandlers>
        <FlatWorldGenerator generatorString="3;7,220*1,5*3,2;3;,biome_1"/>
        <DrawingDecorator>
          <DrawCuboid colour="RED" face="UP" type="carpet" x1="-50" x2="50" y1="226" y2="226" z1="-50" z2="50"/>
          <DrawItem type="egg" x="-41" y="250" z="-46"/>
          <DrawItem type="egg" x="-33" y="250" z="-18"/>
          <DrawItem type="egg" x="2" y="250" z="-19"/>
          <DrawItem type="carrot" x="-28" y="250" z="-44"/>
          <DrawItem type="apple" x="41" y="250" z="30"/>
          <DrawItem type="apple" x="-23" y="250" z="18"/>
          <DrawItem type="potato" x="-28" y="250" z="25"/>
          <DrawItem type="pumpkin_pie" x="-12" y="250" z="3"/>
          <DrawItem type="sugar" x="-9" y="250" z="10"/>
          <DrawItem type="sugar" x="2" y="250" z="46"/>
          <DrawItem type="chicken" x="-3" y="250" z="29"/>
          <DrawItem type="egg" x="33" y="250" z="13"/>
          <DrawItem type="carrot" x="0" y="250" z="-1"/>
          <DrawItem type="cake" x="-26" y="250" z="-14"/>
          <DrawItem type="potato" x="49" y="250" z="-5"/>
          <DrawItem type="potato" x="-48" y="250" z="41"/>
          <DrawItem type="potato" x="-48" y="250" z="-12"/>
          <DrawItem type="potato" x="-7" y="250" z="-42"/>
          <DrawItem type="beef" x="26" y="250" z="9"/>
          <DrawItem type="pumpkin_pie" x="-7" y="250" z="-40"/>
          <DrawItem type="sugar" x="-2" y="250" z="-48"/>
          <DrawItem type="egg" x="7" y="250" z="-27"/>
          <DrawItem type="pumpkin_pie" x="17" y="250" z="38"/>
          <DrawItem type="porkchop" x="-8" y="250" z="-6"/>
          <DrawItem type="egg" x="4" y="250" z="31"/>
          <DrawItem type="chicken" x="-49" y="250" z="-11"/>
          <DrawItem type="pumpkin_pie" x="-1" y="250" z="11"/>
          <DrawItem type="apple" x="-6" y="250" z="-35"/>
          <DrawItem type="fish" x="-38" y="250" z="18"/>
          <DrawItem type="cookie" x="34" y="250" z="5"/>
          <DrawItem type="fish" x="46" y="250" z="-11"/>
          <DrawItem type="fish" x="-21" y="250" z="-29"/>
          <DrawItem type="carrot" x="-10" y="250" z="-47"/>
          <DrawItem type="cake" x="-26" y="250" z="26"/>
          <DrawItem type="pumpkin_pie" x="-25" y="250" z="-13"/>
          <DrawItem type="fish" x="43" y="250" z="2"/>
          <DrawItem type="apple" x="46" y="250" z="47"/>
          <DrawItem type="mutton" x="15" y="250" z="-48"/>
          <DrawItem type="beef" x="46" y="250" z="-1"/>
          <DrawItem type="carrot" x="16" y="250" z="6"/>
          <DrawItem type="mutton" x="-27" y="250" z="26"/>
          <DrawItem type="beef" x="35" y="250" z="-13"/>
          <DrawItem type="chicken" x="44" y="250" z="3"/>
          <DrawItem type="rabbit" x="-44" y="250" z="42"/>
          <DrawItem type="egg" x="43" y="250" z="15"/>
          <DrawItem type="rabbit" x="33" y="250" z="-37"/>
          <DrawItem type="fish" x="-9" y="250" z="-25"/>
          <DrawItem type="melon" x="-45" y="250" z="-21"/>
          <DrawItem type="mutton" x="-13" y="250" z="-40"/>
          <DrawItem type="cookie" x="-7" y="250" z="-31"/>
          <DrawItem type="cookie" x="32" y="250" z="-22"/>
          <DrawItem type="rabbit" x="15" y="250" z="16"/>
          <DrawItem type="mutton" x="-37" y="250" z="26"/>
          <DrawItem type="cookie" x="19" y="250" z="47"/>
          <DrawItem type="beef" x="33" y="250" z="-22"/>
          <DrawItem type="melon" x="19" y="250" z="-25"/>
          <DrawItem type="sugar" x="8" y="250" z="10"/>
          <DrawItem type="egg" x="-41" y="250" z="32"/>
          <DrawItem type="beef" x="49" y="250" z="-18"/>
          <DrawItem type="beef" x="-26" y="250" z="8"/>
          <DrawItem type="porkchop" x="-49" y="250" z="19"/>
          <DrawItem type="beef" x="-34" y="250" z="39"/>
          <DrawItem type="potato" x="-26" y="250" z="-24"/>
          <DrawItem type="melon" x="37" y="250" z="-17"/>
          <DrawItem type="melon" x="-49" y="250" z="-49"/>
          <DrawItem type="potato" x="-45" y="250" z="23"/>
          <DrawItem type="potato" x="40" y="250" z="-18"/>
          <DrawItem type="cake" x="-5" y="250" z="-49"/>
          <DrawItem type="chicken" x="34" y="250" z="-9"/>
          <DrawItem type="sugar" x="-8" y="250" z="44"/>
          <DrawItem type="egg" x="-7" y="250" z="-22"/>
          <DrawItem type="carrot" x="20" y="250" z="36"/>
          <DrawItem type="egg" x="-21" y="250" z="-9"/>
          <DrawItem type="sugar" x="50" y="250" z="22"/>
          <DrawItem type="apple" x="31" y="250" z="-13"/>
          <DrawItem type="mutton" x="-46" y="250" z="-23"/>
          <DrawItem type="cake" x="19" y="250" z="-8"/>
          <DrawItem type="beef" x="29" y="250" z="-43"/>
          <DrawItem type="pumpkin_pie" x="-43" y="250" z="37"/>
          <DrawItem type="chicken" x="-12" y="250" z="45"/>
          <DrawItem type="beef" x="-19" y="250" z="2"/>
          <DrawItem type="egg" x="-32" y="250" z="-13"/>
          <DrawItem type="egg" x="41" y="250" z="13"/>
          <DrawItem type="apple" x="34" y="250" z="-13"/>
          <DrawItem type="carrot" x="25" y="250" z="49"/>
          <DrawItem type="rabbit" x="-10" y="250" z="24"/>
          <DrawItem type="chicken" x="45" y="250" z="-27"/>
          <DrawItem type="egg" x="-22" y="250" z="44"/>
          <DrawItem type="melon" x="44" y="250" z="39"/>
          <DrawItem type="beef" x="48" y="250" z="-8"/>
          <DrawItem type="cake" x="-45" y="250" z="18"/>
          <DrawItem type="fish" x="-27" y="250" z="34"/>
          <DrawItem type="potato" x="24" y="250" z="-18"/>
          <DrawItem type="melon" x="50" y="250" z="43"/>
          <DrawItem type="egg" x="-23" y="250" z="34"/>
          <DrawItem type="carrot" x="-11" y="250" z="27"/>
          <DrawItem type="mutton" x="28" y="250" z="-44"/>
          <DrawItem type="rabbit" x="-47" y="250" z="-29"/>
          <DrawItem type="fish" x="17" y="250" z="-5"/>
          <DrawItem type="porkchop" x="-42" y="250" z="25"/>
          <DrawItem type="sugar" x="26" y="250" z="48"/>
          <DrawItem type="melon" x="44" y="250" z="-26"/>
          <DrawItem type="chicken" x="-14" y="250" z="-33"/>
          <DrawItem type="egg" x="17" y="250" z="7"/>
          <DrawItem type="cookie" x="-36" y="250" z="40"/>
          <DrawItem type="cookie" x="-32" y="250" z="28"/>
          <DrawItem type="rabbit" x="48" y="250" z="-18"/>
          <DrawItem type="porkchop" x="24" y="250" z="-24"/>
          <DrawItem type="melon" x="-13" y="250" z="-17"/>
          <DrawItem type="rabbit" x="-25" y="250" z="38"/>
          <DrawItem type="porkchop" x="18" y="250" z="47"/>
          <DrawItem type="porkchop" x="-47" y="250" z="34"/>
          <DrawItem type="mutton" x="-11" y="250" z="10"/>
          <DrawItem type="potato" x="-17" y="250" z="-38"/>
          <DrawItem type="mutton" x="-42" y="250" z="34"/>
          <DrawItem type="potato" x="-4" y="250" z="-33"/>
          <DrawItem type="mutton" x="-10" y="250" z="39"/>
          <DrawItem type="fish" x="44" y="250" z="-38"/>
          <DrawItem type="apple" x="-11" y="250" z="34"/>
          <DrawItem type="porkchop" x="-30" y="250" z="-4"/>
          <DrawItem type="melon" x="11" y="250" z="19"/>
          <DrawItem type="melon" x="36" y="250" z="2"/>
          <DrawItem type="carrot" x="32" y="250" z="-41"/>
          <DrawItem type="cookie" x="32" y="250" z="-39"/>
          <DrawItem type="melon" x="26" y="250" z="-27"/>
          <DrawItem type="mutton" x="46" y="250" z="-12"/>
          <DrawItem type="porkchop" x="32" y="250" z="-41"/>
          <DrawItem type="potato" x="-40" y="250" z="11"/>
          <DrawItem type="egg" x="-1" y="250" z="-13"/>
          <DrawItem type="fish" x="4" y="250" z="2"/>
          <DrawItem type="chicken" x="-9" y="250" z="22"/>
          <DrawItem type="pumpkin_pie" x="31" y="250" z="5"/>
          <DrawItem type="apple" x="-13" y="250" z="-38"/>
          <DrawItem type="rabbit" x="4" y="250" z="38"/>
          <DrawItem type="potato" x="-2" y="250" z="45"/>
          <DrawItem type="potato" x="7" y="250" z="-18"/>
          <DrawItem type="sugar" x="-34" y="250" z="45"/>
          <DrawItem type="sugar" x="31" y="250" z="-30"/>
          <DrawItem type="egg" x="-26" y="250" z="29"/>
          <DrawItem type="potato" x="13" y="250" z="37"/>
          <DrawItem type="beef" x="-45" y="250" z="-33"/>
          <DrawItem type="carrot" x="20" y="250" z="29"/>
          <DrawItem type="potato" x="-48" y="250" z="-41"/>
          <DrawItem type="sugar" x="-11" y="250" z="-22"/>
          <DrawItem type="melon" x="-48" y="250" z="25"/>
          <DrawItem type="egg" x="-32" y="250" z="-24"/>
          <DrawItem type="chicken" x="-46" y="250" z="-12"/>
          <DrawItem type="sugar" x="17" y="250" z="34"/>
          <DrawItem type="mutton" x="-37" y="250" z="-44"/>
          <DrawItem type="carrot" x="-15" y="250" z="26"/>
          <DrawItem type="egg" x="-48" y="250" z="40"/>
          <DrawItem type="melon" x="-22" y="250" z="-6"/>
          <DrawItem type="sugar" x="-4" y="250" z="33"/>
          <DrawItem type="pumpkin_pie" x="24" y="250" z="29"/>
          <DrawItem type="beef" x="-16" y="250" z="22"/>
          <DrawItem type="beef" x="-50" y="250" z="6"/>
          <DrawItem type="porkchop" x="19" y="250" z="30"/>
          <DrawItem type="carrot" x="-49" y="250" z="-32"/>
          <DrawItem type="cookie" x="3" y="250" z="-27"/>
          <DrawItem type="melon" x="46" y="250" z="13"/>
          <DrawItem type="porkchop" x="-32" y="250" z="-3"/>
          <DrawItem type="mutton" x="-33" y="250" z="-40"/>
          <DrawItem type="cookie" x="43" y="250" z="7"/>
          <DrawItem type="carrot" x="32" y="250" z="-13"/>
          <DrawItem type="chicken" x="-50" y="250" z="-50"/>
          <DrawItem type="cookie" x="32" y="250" z="-15"/>
          <DrawItem type="sugar" x="30" y="250" z="34"/>
          <DrawItem type="mutton" x="-7" y="250" z="8"/>
          <DrawItem type="chicken" x="31" y="250" z="-42"/>
          <DrawItem type="chicken" x="-36" y="250" z="31"/>
          <DrawItem type="egg" x="-5" y="250" z="49"/>
          <DrawItem type="cookie" x="-31" y="250" z="-45"/>
          <DrawItem type="carrot" x="38" y="250" z="50"/>
          <DrawItem type="chicken" x="0" y="250" z="26"/>
          <DrawItem type="apple" x="43" y="250" z="1"/>
          <DrawItem type="beef" x="-46" y="250" z="30"/>
          <DrawItem type="sugar" x="16" y="250" z="24"/>
          <DrawItem type="beef" x="-50" y="250" z="21"/>
          <DrawItem type="sugar" x="-14" y="250" z="20"/>
          <DrawItem type="mutton" x="-29" y="250" z="24"/>
          <DrawItem type="melon" x="-26" y="250" z="24"/>
          <DrawItem type="beef" x="-2" y="250" z="-16"/>
          <DrawItem type="melon" x="-30" y="250" z="42"/>
          <DrawItem type="rabbit" x="38" y="250" z="-48"/>
          <DrawItem type="rabbit" x="35" y="250" z="5"/>
          <DrawItem type="carrot" x="-11" y="250" z="-25"/>
          <DrawItem type="porkchop" x="30" y="250" z="-27"/>
          <DrawItem type="carrot" x="-6" y="250" z="-37"/>
          <DrawItem type="carrot" x="-42" y="250" z="22"/>
          <DrawItem type="egg" x="50" y="250" z="-5"/>
          <DrawItem type="carrot" x="20" y="250" z="-5"/>
          <DrawItem type="beef" x="19" y="250" z="33"/>
          <DrawItem type="cookie" x="-28" y="250" z="17"/>
          <DrawItem type="mutton" x="25" y="250" z="16"/>
          <DrawItem type="cake" x="-23" y="250" z="-26"/>
          <DrawItem type="rabbit" x="49" y="250" z="11"/>
          <DrawItem type="potato" x="-36" y="250" z="-13"/>
          <DrawItem type="egg" x="5" y="250" z="18"/>
          <DrawItem type="cake" x="30" y="250" z="21"/>
          <DrawItem type="porkchop" x="46" y="250" z="-49"/>
          <DrawItem type="potato" x="-1" y="250" z="6"/>
          <DrawItem type="sugar" x="-4" y="250" z="20"/>
          <DrawItem type="cookie" x="-22" y="250" z="-44"/>
          <DrawItem type="cookie" x="-43" y="250" z="-20"/>
          <DrawItem type="rabbit" x="27" y="250" z="23"/>
          <DrawItem type="porkchop" x="12" y="250" z="32"/>
          <DrawItem type="melon" x="10" y="250" z="-22"/>
          <DrawItem type="potato" x="6" y="250" z="-38"/>
          <DrawItem type="cookie" x="-1" y="250" z="17"/>
          <DrawItem type="mutton" x="-36" y="250" z="-21"/>
          <DrawItem type="cake" x="5" y="250" z="37"/>
          <DrawItem type="beef" x="26" y="250" z="-49"/>
          <DrawItem type="fish" x="25" y="250" z="-34"/>
          <DrawItem type="melon" x="30" y="250" z="23"/>
          <DrawItem type="sugar" x="-33" y="250" z="35"/>
          <DrawItem type="carrot" x="-34" y="250" z="-11"/>
          <DrawItem type="beef" x="32" y="250" z="-38"/>
          <DrawItem type="egg" x="40" y="250" z="20"/>
          <DrawItem type="pumpkin_pie" x="26" y="250" z="-27"/>
          <DrawItem type="mutton" x="23" y="250" z="-35"/>
          <DrawItem type="cookie" x="26" y="250" z="15"/>
          <DrawItem type="rabbit" x="-43" y="250" z="33"/>
          <DrawItem type="cake" x="8" y="250" z="-46"/>
          <DrawItem type="fish" x="12" y="250" z="34"/>
          <DrawItem type="beef" x="-26" y="250" z="-23"/>
          <DrawItem type="chicken" x="-9" y="250" z="-3"/>
          <DrawItem type="porkchop" x="-5" y="250" z="42"/>
          <DrawItem type="rabbit" x="-18" y="250" z="-48"/>
          <DrawItem type="apple" x="16" y="250" z="50"/>
          <DrawItem type="cake" x="-37" y="250" z="-40"/>
          <DrawItem type="carrot" x="-42" y="250" z="18"/>
          <DrawItem type="cake" x="-13" y="250" z="26"/>
          <DrawItem type="beef" x="-17" y="250" z="3"/>
          <DrawItem type="potato" x="50" y="250" z="-20"/>
          <DrawItem type="beef" x="20" y="250" z="-26"/>
          <DrawItem type="cake" x="50" y="250" z="-28"/>
          <DrawItem type="porkchop" x="-49" y="250" z="-24"/>
          <DrawItem type="pumpkin_pie" x="9" y="250" z="-33"/>
          <DrawItem type="potato" x="0" y="250" z="1"/>
          <DrawItem type="carrot" x="-7" y="250" z="-47"/>
          <DrawItem type="mutton" x="21" y="250" z="37"/>
          <DrawItem type="cake" x="25" y="250" z="-32"/>
          <DrawItem type="chicken" x="6" y="250" z="23"/>
          <DrawItem type="fish" x="-28" y="250" z="-17"/>
          <DrawItem type="potato" x="-45" y="250" z="-46"/>
          <DrawItem type="beef" x="-28" y="250" z="1"/>
          <DrawItem type="cookie" x="-11" y="250" z="-14"/>
          <DrawItem type="pumpkin_pie" x="-28" y="250" z="21"/>
          <DrawItem type="mutton" x="11" y="250" z="14"/>
          <DrawItem type="carrot" x="12" y="250" z="41"/>
          <DrawItem type="cookie" x="13" y="250" z="-31"/>
          <DrawItem type="pumpkin_pie" x="-29" y="250" z="47"/>
          <DrawItem type="melon" x="-4" y="250" z="45"/>
          <DrawItem type="beef" x="-41" y="250" z="-37"/>
          <DrawItem type="potato" x="50" y="250" z="33"/>
          <DrawItem type="porkchop" x="-33" y="250" z="-31"/>
          <DrawItem type="pumpkin_pie" x="19" y="250" z="3"/>
          <DrawItem type="mutton" x="-10" y="250" z="-35"/>
          <DrawItem type="cookie" x="46" y="250" z="2"/>
          <DrawItem type="carrot" x="2" y="250" z="40"/>
          <DrawItem type="chicken" x="0" y="250" z="2"/>
          <DrawItem type="pumpkin_pie" x="13" y="250" z="-5"/>
          <DrawItem type="beef" x="15" y="250" z="41"/>
          <DrawItem type="rabbit" x="-24" y="250" z="-9"/>
          <DrawItem type="melon" x="-14" y="250" z="-33"/>
          <DrawItem type="beef" x="10" y="250" z="-11"/>
          <DrawItem type="pumpkin_pie" x="25" y="250" z="8"/>
          <DrawItem type="porkchop" x="-30" y="250" z="11"/>
          <DrawItem type="fish" x="23" y="250" z="6"/>
          <DrawItem type="pumpkin_pie" x="-13" y="250" z="41"/>
          <DrawItem type="fish" x="40" y="250" z="-43"/>
          <DrawItem type="egg" x="-20" y="250" z="31"/>
          <DrawItem type="apple" x="-38" y="250" z="-47"/>
          <DrawItem type="rabbit" x="-5" y="250" z="-28"/>
          <DrawItem type="pumpkin_pie" x="18" y="250" z="23"/>
          <DrawItem type="melon" x="-29" y="250" z="-19"/>
          <DrawItem type="beef" x="-35" y="250" z="0"/>
          <DrawItem type="cookie" x="-12" y="250" z="10"/>
          <DrawItem type="potato" x="28" y="250" z="36"/>
          <DrawItem type="mutton" x="31" y="250" z="9"/>
          <DrawItem type="pumpkin_pie" x="-39" y="250" z="-23"/>
          <DrawItem type="rabbit" x="-21" y="250" z="-27"/>
          <DrawItem type="potato" x="8" y="250" z="34"/>
          <DrawItem type="mutton" x="1" y="250" z="12"/>
          <DrawItem type="melon" x="1" y="250" z="-27"/>
          <DrawItem type="egg" x="22" y="250" z="28"/>
          <DrawItem type="cookie" x="-47" y="250" z="-25"/>
          <DrawItem type="mutton" x="-11" y="250" z="14"/>
          <DrawItem type="apple" x="20" y="250" z="4"/>
          <DrawItem type="pumpkin_pie" x="-31" y="250" z="44"/>
          <DrawItem type="sugar" x="-14" y="250" z="-45"/>
          <DrawItem type="porkchop" x="24" y="250" z="19"/>
          <DrawItem type="beef" x="40" y="250" z="-1"/>
          <DrawItem type="mutton" x="37" y="250" z="0"/>
          <DrawItem type="cookie" x="-48" y="250" z="-23"/>
          <DrawItem type="rabbit" x="-15" y="250" z="23"/>
          <DrawItem type="carrot" x="13" y="250" z="-11"/>
          <DrawItem type="potato" x="4" y="250" z="31"/>
          <DrawItem type="fish" x="29" y="250" z="-18"/>
          <DrawItem type="rabbit" x="-41" y="250" z="41"/>
          <DrawItem type="egg" x="16" y="250" z="13"/>
          <DrawItem type="melon" x="-41" y="250" z="-40"/>
          <DrawItem type="chicken" x="7" y="250" z="20"/>
          <DrawItem type="apple" x="17" y="250" z="-43"/>
          <DrawItem type="carrot" x="34" y="250" z="23"/>
          <DrawItem type="melon" x="-49" y="250" z="-5"/>
          <DrawItem type="potato" x="-39" y="250" z="18"/>
          <DrawItem type="porkchop" x="-39" y="250" z="-15"/>
          <DrawItem type="rabbit" x="44" y="250" z="34"/>
          <DrawItem type="fish" x="-35" y="250" z="34"/>
          <DrawItem type="sugar" x="43" y="250" z="46"/>
          <DrawItem type="apple" x="-6" y="250" z="31"/>
          <DrawItem type="rabbit" x="13" y="250" z="5"/>
          <DrawItem type="egg" x="-17" y="250" z="-33"/>
          <DrawItem type="porkchop" x="-2" y="250" z="14"/>
          <DrawItem type="porkchop" x="31" y="250" z="-22"/>
          <DrawItem type="melon" x="46" y="250" z="-35"/>
          <DrawItem type="porkchop" x="-42" y="250" z="-23"/>
          <DrawItem type="cake" x="-45" y="250" z="-42"/>
          <DrawItem type="cookie" x="5" y="250" z="-2"/>
          <DrawItem type="fish" x="47" y="250" z="29"/>
          <DrawItem type="fish" x="39" y="250" z="-20"/>
          <DrawItem type="cake" x="-29" y="250" z="16"/>
          <DrawItem type="carrot" x="33" y="250" z="8"/>
          <DrawItem type="fish" x="-47" y="250" z="6"/>
          <DrawItem type="cookie" x="-2" y="250" z="13"/>
          <DrawItem type="pumpkin_pie" x="25" y="250" z="-2"/>
          <DrawItem type="apple" x="-39" y="250" z="23"/>
          <DrawItem type="fish" x="37" y="250" z="4"/>
          <DrawItem type="fish" x="8" y="250" z="37"/>
          <DrawItem type="cookie" x="-29" y="250" z="38"/>
          <DrawItem type="cookie" x="-39" y="250" z="-5"/>
          <DrawItem type="pumpkin_pie" x="17" y="250" z="-38"/>
          <DrawItem type="carrot" x="-38" y="250" z="-34"/>
          <DrawItem type="porkchop" x="-17" y="250" z="2"/>
          <DrawItem type="pumpkin_pie" x="6" y="250" z="0"/>
          <DrawItem type="beef" x="43" y="250" z="-36"/>
          <DrawItem type="carrot" x="1" y="250" z="3"/>
          <DrawItem type="apple" x="-23" y="250" z="-14"/>
          <DrawItem type="mutton" x="-19" y="250" z="30"/>
          <DrawItem type="fish" x="12" y="250" z="-3"/>
          <DrawItem type="porkchop" x="38" y="250" z="14"/>
          <DrawItem type="apple" x="7" y="250" z="28"/>
          <DrawItem type="cake" x="9" y="250" z="-5"/>
          <DrawItem type="melon" x="-49" y="250" z="22"/>
          <DrawItem type="fish" x="-39" y="250" z="-42"/>
          <DrawItem type="rabbit" x="23" y="250" z="22"/>
          <DrawItem type="rabbit" x="8" y="250" z="-14"/>
          <DrawItem type="chicken" x="-10" y="250" z="-19"/>
          <DrawItem type="porkchop" x="-1" y="250" z="8"/>
          <DrawItem type="pumpkin_pie" x="8" y="250" z="23"/>
          <DrawItem type="cookie" x="34" y="250" z="-1"/>
          <DrawItem type="carrot" x="32" y="250" z="-5"/>
          <DrawItem type="egg" x="50" y="250" z="-29"/>
          <DrawItem type="cake" x="40" y="250" z="31"/>
          <DrawItem type="pumpkin_pie" x="48" y="250" z="23"/>
          <DrawItem type="rabbit" x="36" y="250" z="17"/>
          <DrawItem type="sugar" x="-10" y="250" z="-31"/>
          <DrawItem type="mutton" x="-25" y="250" z="-31"/>
          <DrawItem type="egg" x="33" y="250" z="42"/>
          <DrawItem type="fish" x="-23" y="250" z="-8"/>
          <DrawItem type="rabbit" x="27" y="250" z="32"/>
          <DrawItem type="apple" x="-41" y="250" z="-37"/>
          <DrawItem type="egg" x="40" y="250" z="-17"/>
          <DrawItem type="apple" x="-41" y="250" z="-32"/>
          <DrawItem type="carrot" x="-16" y="250" z="42"/>
          <DrawItem type="cake" x="-16" y="250" z="45"/>
          <DrawItem type="mutton" x="-35" y="250" z="38"/>
          <DrawItem type="fish" x="-45" y="250" z="38"/>
          <DrawItem type="apple" x="-7" y="250" z="31"/>
          <DrawItem type="apple" x="0" y="250" z="-36"/>
          <DrawItem type="mutton" x="-46" y="250" z="2"/>
          <DrawItem type="cake" x="28" y="250" z="46"/>
          <DrawItem type="melon" x="33" y="250" z="-38"/>
          <DrawItem type="fish" x="37" y="250" z="-23"/>
          <DrawItem type="chicken" x="-32" y="250" z="-46"/>
          <DrawItem type="potato" x="26" y="250" z="-44"/>
          <DrawItem type="melon" x="-16" y="250" z="-48"/>
          <DrawItem type="porkchop" x="50" y="250" z="44"/>
          <DrawItem type="mutton" x="-34" y="250" z="-50"/>
          <DrawItem type="pumpkin_pie" x="38" y="250" z="24"/>
          <DrawItem type="mutton" x="-10" y="250" z="-1"/>
          <DrawItem type="pumpkin_pie" x="41" y="250" z="-10"/>
          <DrawItem type="beef" x="2" y="250" z="-50"/>
          <DrawItem type="sugar" x="42" y="250" z="-50"/>
          <DrawItem type="beef" x="-47" y="250" z="20"/>
          <DrawItem type="mutton" x="-22" y="250" z="46"/>
          <DrawItem type="cookie" x="28" y="250" z="-48"/>
          <DrawItem type="mutton" x="-7" y="250" z="20"/>
          <DrawItem type="egg" x="48" y="250" z="41"/>
          <DrawItem type="egg" x="49" y="250" z="27"/>
          <DrawItem type="apple" x="-20" y="250" z="14"/>
          <DrawItem type="fish" x="-13" y="250" z="-21"/>
          <DrawItem type="carrot" x="-12" y="250" z="-24"/>
          <DrawItem type="rabbit" x="-7" y="250" z="9"/>
          <DrawItem type="pumpkin_pie" x="15" y="250" z="48"/>
          <DrawItem type="beef" x="30" y="250" z="-23"/>
          <DrawItem type="cookie" x="50" y="250" z="5"/>
          <DrawItem type="chicken" x="-33" y="250" z="-25"/>
          <DrawItem type="cake" x="18" y="250" z="-13"/>
        </DrawingDecorator>
        <ServerQuitFromTimeUp description="" timeLimitMs="15000"/>
        <ServerQuitWhenAnyAgentFinishes description=""/>
      </ServerHandlers>
    </ServerSection>
    
    <AgentSection mode="Survival">
      <Name>The Hungry Caterpillar</Name>
      <AgentStart>
        <Placement pitch="0" x="0" y="226.0" yaw="0" z="0"/>
        <Inventory/>
      </AgentStart>
      <AgentHandlers>
        <VideoProducer want_depth="false">
          <Width>480</Width>
          <Height>320</Height>
        </VideoProducer>
        <RewardForCollectingItem>
          <Item reward="2" type="fish porkchop beef chicken rabbit mutton"/>
          <Item reward="1" type="potato egg carrot"/>
          <Item reward="-1" type="apple melon"/>
          <Item reward="-2" type="sugar cake cookie pumpkin_pie"/>
        </RewardForCollectingItem>
        <ContinuousMovementCommands turnSpeedDegs="240">
          <ModifierList type="deny-list">
            <command>attack</command>
          </ModifierList>
        </ContinuousMovementCommands>
      </AgentHandlers>
    </AgentSection>
  </Mission>
'''

epochs = 3
current_epoch = 0

# Create default Malmo objects:

agent_host = MalmoPython.AgentHost()
try:
    agent_host.parse( sys.argv)
except RuntimeError as e:
    print('ERROR:', e)
    print(agent_host.getUsage())
    exit(1)
if agent_host.receivedArgument("help"):
    print(agent_host.getUsage())
    exit(0)


for epoch in range(epochs):

    actions = []
    states = []
    cumul_reward = 0
    d = False

    my_mission = MalmoPython.MissionSpec(missionXML, True)
    my_mission_record = MalmoPython.MissionRecordSpec()

    # Attempt to start a mission:
    max_retries = 3
    for retry in range(max_retries):
        try:
            agent_host.startMission( my_mission, my_mission_record)
            break
        except RuntimeError as e:
            if retry == max_retries - 1:
                print("Error starting mission:", e)
                exit(1)
            else:
                time.sleep(2)

    # Loop until mission starts:
    print("Waiting for the mission to start ", end=' ')
    world_state = agent_host.getWorldState()
    while not world_state.has_mission_begun:
        print(".", end="")
        time.sleep(0.1)
        world_state = agent_host.getWorldState()
        for error in world_state.errors:
            print("Error:", error.text)

    print()
    print("Mission running ", end=' ')

    # Loop until mission ends:
    i = 0
    while world_state.is_mission_running:


        if i > 10:
            agent_host.sendCommand("move 1")

        else:
            agent_host.sendCommand("move 0")

        # on choisit une action aléatoire avec une certaine probabilité, qui décroit
        # TODO : simplifier ça (pas clair)
        # Q2 = Q[s, :] + np.random.randn(1, actions_n)*(1. / (current_epoch + 1))
        # a = np.argmax(Q2)
        # print("-----------------------------------------------------------------------")
        # print(a)
        # print("-----------------------------------------------------------------------")
        # s1, reward, d, _ = agent_host.sendCommand(a)
        # Q[s, a] = Q[s, a] + lr*(reward + y * np.max(Q[s1,:]) - Q[s, a]) # Fonction de mise à jour de la Q-table
        # cumul_reward += reward
        # s = s1
        # actions.append(a)
        # states.append(s)

        print(".", end="")
        time.sleep(0.1)
        i += 1
        world_state = agent_host.getWorldState()

        print(world_state.observations[-1].text)
        for error in world_state.errors:
            print("Error:", error.text)

    print()
    print("Mission ended")
    current_epoch += 1
    # Mission has ended.


#print("Score over time: " + str(sum(cumul_reward_list[-100:])/100.0))
